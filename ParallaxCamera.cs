//Script to set up parallax scrolling in Unity
//Milam Dobbins 2013
//To implement this, instantiate a new ParallaxCamera object with the base speed as a parameter,
//and then call it in the update(or fixed update) method, giving two references to two different gameobjects holding the background images, 
//along with a player velocity.  Remember to have the player tagged as a player.

using UnityEngine;
using System.Collections;

public class ParallaxCamera{
	private float moveParallax = 0;
	private float speed = 0;
	private Vector2 pSpeed;

	

	public ParallaxCamera(float setSpeed)//Default Constructor. 
	{
		speed = setSpeed;
	}


	public void ParallaxScroll(GameObject firstBackground, GameObject secondBackground, float velocitySpeed)
	{

		pSpeed = GameObject.FindGameObjectWithTag("Player").rigidbody2D.velocity;
		string trackS = pSpeed.ToString();
		//Debug.Log(trackS);  //(pSpeed.ToString);

		if((velocitySpeed > 0||velocitySpeed < 0)&&(Input.GetAxis ("Horizontal")!=0))
		{
			if(Input.GetAxis("Horizontal")>0){
				firstBackground.renderer.material.mainTextureOffset = new Vector2((moveParallax * speed)%1, 0f);
				secondBackground.renderer.material.mainTextureOffset = new Vector2((moveParallax * (speed/5.0f))%1, 0f);
				moveParallax+=.0005f;
			}else if((Input.GetAxis("Horizontal")<0)&&velocitySpeed<0)
			{
				firstBackground.renderer.material.mainTextureOffset = new Vector2(((speed*moveParallax)%1), 0f);
				secondBackground.renderer.material.mainTextureOffset = new Vector2((moveParallax * (speed/5.0f))%1, 0f);
				moveParallax-=.0005f;
				
			}else{
				return;
			}
			
		}
	}


	}




