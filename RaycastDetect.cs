//Raycast class overloaded for multiple options
//Milam Dobbins 2013
using UnityEngine;
using System.Collections;



public class RaycastDetect{
	
	public bool rayDetected = false;

	public RaycastDetect()
	{
		rayDetected = false;
		//Defaulr constructor
	}

	public bool RaycastDetection(Transform rayStartingPoint, Transform rayEndPoint, string rayLayerToDetect, bool debug)
	{	if(debug){
		Debug.DrawLine(rayStartingPoint.position,rayEndPoint.position, Color.green);
		}
		rayDetected = Physics2D.Linecast(rayStartingPoint.position, rayEndPoint.position, 1<< LayerMask.NameToLayer(rayLayerToDetect));

		return rayDetected;
	}
	

	
	public bool RaycastDetection(Transform[] rayStartingPoint, Transform[] rayEndPoint, string[] rayLayerToDetect)
	{   
		
		int rayArraySize1 = rayStartingPoint.GetLength(0);		
		int rayArraySize2 = rayEndPoint.GetLength(0);		
		int rayArraySize3 = rayLayerToDetect.GetLength(0);
		
		for(int i =0; i <rayArraySize1; i++)
		{
			int j = 0;

			for(int k = 0;k < rayArraySize3;k++)
			{
			rayDetected = Physics2D.Linecast(rayStartingPoint[i].position, rayEndPoint[j].position, 1<< LayerMask.NameToLayer(rayLayerToDetect[k]));
			
			if(rayDetected)return rayDetected;
			}

			if(j < rayArraySize2)j++;
			
		}
		
		return rayDetected;
	} 

	

	public bool RaycastDetection(Transform[] rayStartingPoint, Transform[] rayEndPoint, string rayLayerToDetect)
	{   
		
		int rayArraySize1 = rayStartingPoint.GetLength(0);		
		int rayArraySize2 = rayEndPoint.GetLength(0);
		
		
		
		for(int i =0; i <rayArraySize1; i++)
		{
			int j = 0;

				Debug.DrawLine(rayStartingPoint[i].position,rayEndPoint[j].position, Color.green);
				rayDetected = Physics2D.Linecast(rayStartingPoint[i].position, rayEndPoint[j].position, 1<< LayerMask.NameToLayer(rayLayerToDetect));
				
				if(rayDetected)return rayDetected;

				if(j < rayArraySize2)j++;
			
		}
		
		return rayDetected;
	} 


	public bool RaycastDetection(Transform[] rayStartingPoint, Transform rayEndPoint, string[] rayLayerToDetect)
	{   
		
		int rayArraySize1 = rayStartingPoint.GetLength(0);
		int rayArraySize3 = rayLayerToDetect.GetLength(0);
			
		for(int i =0; i <rayArraySize1; i++)
		{
			for(int k = 0;k < rayArraySize3;k++)
			{
				rayDetected = Physics2D.Linecast(rayStartingPoint[i].position, rayEndPoint.position, 1<< LayerMask.NameToLayer(rayLayerToDetect[k]));				
				if(rayDetected)return rayDetected;
			}		
		}
		
		return rayDetected;
	} 



	public bool RaycastDetection(Transform[] rayStartingPoint, Transform rayEndPoint, string rayLayerToDetect)
	{   
		
		int rayArraySize1 = rayStartingPoint.GetLength(0);
		
		
		for(int i =0; i <rayArraySize1; i++)
		{
				rayDetected = Physics2D.Linecast(rayStartingPoint[i].position, rayEndPoint.position, 1<< LayerMask.NameToLayer(rayLayerToDetect));
				
				if(rayDetected)return rayDetected;			
		}
		
		return rayDetected;
	} 



	public bool RaycastDetection(Transform rayStartingPoint, Transform rayEndPoint, string rayLayerToDetect)
	{	
		rayDetected = Physics2D.Linecast(rayStartingPoint.position, rayEndPoint.position, 1<< LayerMask.NameToLayer(rayLayerToDetect));
		
		return rayDetected;
	}



}
	

