//Script to control player in 2D platformer that will activate running, jumping/falling, and shooting animations
//Milam Dobbins 2013
using UnityEngine;
using System.Collections;


public class PlayerControllerScript : MonoBehaviour {
	//
	//Declare class objects
	private ParallaxCamera scrollControl;
	private RaycastDetect raycastDetector;
	private GameObject mainCamera;
	public GameObject Bullet;
	public Transform BulletStartingPoint;


	//Declare arrays of transforms needing to be raycast
	public Transform[] startOfRaycastsArray;
	public Transform[] endOfRaycastArray;


	//properties to control speed, audio, and facing
	public float maxSpeed;
	private bool facingRight = true;
	public AudioClip jumpAudio;

	//Declare background objects
	public GameObject firstBackground;
	public GameObject secondBackground;

	//Declare needed properties for animation
	private Animator anim;

	//Falling-Jumping
	bool grounded = false;
	bool crouching = false;
	public Transform groundCheck;
	float groundRadius = .02f;
	public LayerMask whatIsGround;

	public float jumpForce = 100f;
	
	//private float rightLimit = 1f;
	//private float leftLimit = 1f;

	bool doubleJump = false;

	// Use this for initialization
	void Start () {

		scrollControl = new ParallaxCamera(.3f);
		raycastDetector = new RaycastDetect();
		anim = GetComponent<Animator>();	
	}
	

	// Update is called once per frame
	void FixedUpdate () 
	{

		//scroll background
		if(rigidbody2D.velocity.x != 0){
			scrollControl.ParallaxScroll(firstBackground, secondBackground,rigidbody2D.velocity.x );
		}

		//Check falling/jumping
		grounded = Physics2D.OverlapCircle(groundCheck.position, groundRadius, whatIsGround);
		anim.SetBool("Ground",grounded);

		if(grounded){
			doubleJump = false;
			anim.SetBool("DoubleJump", false);
		}

		/* //use this if you don't want crawling
		if(crouching&&grounded)
		{
			anim.SetFloat("Speed", 0);
			return;
		}*/

		anim.SetFloat("vSpeed", rigidbody2D.velocity.y);


		//if(!grounded)return; //prevents moving in air

		float move = Input.GetAxis("Horizontal");
		//Debug.Log("%s",move.ToString);

		anim.SetFloat("Speed", Mathf.Abs (move));

		rigidbody2D.velocity = new Vector2(move*maxSpeed, rigidbody2D.velocity.y);
	
	
		if(move > 0 &&!facingRight)
		{
			Flip ();
		}
		else if(move < 0 && facingRight)
		{
			Flip ();
		}

		if(raycastDetector.RaycastDetection(startOfRaycastsArray,endOfRaycastArray,"Default"))
		{
			anim.SetBool("Detected", true);
		}else{
			anim.SetBool("Detected", false);
		}
	
	}

//Update checks continuously 
	void Update()
	{	

		Shooting();
		PlayerActions();

	}


	void Flip()//Flip animations
	{
		facingRight = !facingRight;
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;	
	}

	void Shooting()
	{

		//Check shooting
		if(Input.GetButtonDown("Fire1"))
		{
			anim.SetBool("Shooting", true);
			Instantiate(Bullet,BulletStartingPoint.position, Quaternion.identity);
			//Bullet.
		}else{
			anim.SetBool("Shooting", false);
			
		}


	}

	void PlayerActions()
	{
		bool testCrouch = Input.GetAxis("Vertical")<0;
		float testSpeed = anim.GetFloat ("Speed");
		
		if(testCrouch&& (testSpeed == 0))
		{
			anim.SetBool("Crouching",true);
			crouching = true;
		}else if(!testCrouch&&crouching){
			anim.SetBool("Crouching", false);
			testCrouch = false;
			crouching = false;
		}
		
		if((grounded||!doubleJump) && Input.GetButtonDown("Jump"))
		{
			if(!testCrouch)
			{
				anim.SetBool("Ground",false);
				rigidbody2D.AddForce(new Vector2(0, jumpForce));
				//audio.Play();
				
				if(!doubleJump&&!grounded)
				{
					doubleJump = true;
					anim.SetBool("DoubleJump", true);
				}
				
				
				if(!doubleJump)
				{
					AudioSource.PlayClipAtPoint(jumpAudio, transform.position);
				}
			}
						
			if(!doubleJump&&(anim.GetBool("Crouching")==true)&&grounded)
			{
				float crouchJumpForce = jumpForce * 1.7f;
				rigidbody2D.AddForce(new Vector2(0, crouchJumpForce));
				crouching = false;
				testCrouch = false;
				anim.SetBool("Crouching", false);
				doubleJump = true;
				AudioSource.PlayClipAtPoint(jumpAudio, transform.position);
				
			}

		}


	}

}
